#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info, debug
from mininet.node import Host, RemoteController, OVSSwitch
import os

QUAGGA_DIR = '/usr/lib/quagga'
# Must exist and be owned by quagga user (quagga:quagga by default on Ubuntu)
QUAGGA_RUN_DIR = '/var/run/quagga'
EXABGP_RUN_EXE = '~/exabgp/sbin/exabgp'
CONFIG_DIR = 'configs/'

onos_controller = RemoteController('c0', ip='192.168.0.1', port=6633)


class Onos(Host):
    '''
        ONOS Host
    '''

    def __init__(self, name, intfDict, *args, **kwargs):
        Host.__init__(self, name, *args, **kwargs)

        self.intfDict = intfDict

    def config(self, **kwargs):
        Host.config(self, **kwargs)

        for intf, attrs in self.intfDict.items():
            self.cmd('ip addr flush dev %s' % intf)
            if 'mac' in attrs:
                self.cmd('ip link set %s down' % intf)
                self.cmd('ip link set %s address %s' % (intf, attrs['mac']))
                self.cmd('ip link set %s up ' % intf)
            for addr in attrs['ipAddrs']:
                self.cmd('ip addr add %s dev %s' % (addr, intf))


class QuaggaRouter(Host):
    '''
        Quagga Router Host
    '''

    def __init__(self, name, quaggaConfFile, zebraConfFile, intfDict, *args, **kwargs):
        Host.__init__(self, name, *args, **kwargs)

        self.quaggaConfFile = quaggaConfFile
        self.zebraConfFile = zebraConfFile
        self.intfDict = intfDict

    def config(self, **kwargs):
        Host.config(self, **kwargs)
        self.cmd('sysctl net.ipv4.ip_forward=1')

        for intf, attrs in self.intfDict.items():
            self.cmd('ip addr flush dev %s' % intf)
            if 'mac' in attrs:
                self.cmd('ip link set %s down' % intf)
                self.cmd('ip link set %s address %s' % (intf, attrs['mac']))
                self.cmd('ip link set %s up ' % intf)
            for addr in attrs['ipAddrs']:
                self.cmd('ip addr add %s dev %s' % (addr, intf))

        self.cmd('/usr/lib/quagga/zebra -d -f %s -z %s/zebra%s.api -i %s/zebra%s.pid' %
                 (self.zebraConfFile, QUAGGA_RUN_DIR, self.name, QUAGGA_RUN_DIR, self.name))
        self.cmd('/usr/lib/quagga/bgpd -d -f %s -z %s/zebra%s.api -i %s/bgpd%s.pid' %
                 (self.quaggaConfFile, QUAGGA_RUN_DIR, self.name, QUAGGA_RUN_DIR, self.name))

    def terminate(self):
        self.cmd("ps ax | egrep 'bgpd%s.pid|zebra%s.pid' | awk '{print $1}' | xargs kill" % (
            self.name, self.name))

        Host.terminate(self)


class ExaBGPRouter(Host):
    '''
        ExaBGP Router Host
    '''

    def __init__(self, name, exaBGPconf, intfDict, *args, **kwargs):
        Host.__init__(self, name, *args, **kwargs)

        self.exaBGPconf = exaBGPconf
        self.intfDict = intfDict

    def config(self, **kwargs):
        Host.config(self, **kwargs)
        self.cmd('sysctl net.ipv4.ip_forward=1')

        for intf, attrs in self.intfDict.items():
            self.cmd('ip addr flush dev %s' % intf)
            if 'mac' in attrs:
                self.cmd('ip link set %s down' % intf)
                self.cmd('ip link set %s address %s' % (intf, attrs['mac']))
                self.cmd('ip link set %s up ' % intf)
            for addr in attrs['ipAddrs']:
                self.cmd('ip addr add %s dev %s' % (addr, intf))

        self.cmd('%s %s > /dev/null 2> exabgp.log &' %
                 (EXABGP_RUN_EXE, self.exaBGPconf))

    def terminate(self):
        self.cmd(
            "ps ax | egrep 'lib/exabgp/application/bgp.py' | awk '{print $1}' | xargs kill")
        self.cmd(
            "ps ax | egrep 'server.py' | awk '{print $1}' | xargs kill")
        Host.terminate(self)


class ONOSSwitch(OVSSwitch):
    '''
        ONOS edge OVS switch
    '''

    def __init__(self, name, intfDict=None, **params):
        OVSSwitch.__init__(self, name, failMode='secure',
                           datapath='kernel', **params)
        self.intfDict = intfDict

    def start(self, controllers):
        for intf, attrs in self.intfDict.items():
            self.cmd('ip addr flush dev %s' % intf)
            if 'mac' in attrs:
                self.cmd('ip link set %s down' % intf)
                self.cmd('ip link set %s address %s' % (intf, attrs['mac']))
                self.cmd('ip link set %s up ' % intf)
            for addr in attrs['ipAddrs']:
                self.cmd('ip addr add %s dev %s' % (addr, intf))

        return OVSSwitch.start(self, [onos_controller])


class LegacySwitch(OVSSwitch):
    '''
        Legacy L2 switch
    '''

    def __init__(self, name, **params):
        OVSSwitch.__init__(self, name, failMode='standalone', **params)
        self.switchIP = None

    def start(self, controllers):
        return OVSSwitch.start(self, [])

if __name__ == '__main__':
    setLogLevel('debug')
    REMOTE_IP = '10.0.2.15'

    net = Mininet(topo=None, build=False)
    net.addController(onos_controller)

    # R1

    zebraConf = '%szebra.conf' % CONFIG_DIR
    quaggaConf = '%sR1-quagga.conf' % CONFIG_DIR
    name = 'R1'
    eth0 = {
        'ipAddrs': ['150.1.1.2/30'],
        'mac': 'e2:f5:32:16:9a:46'
    }
    eth1 = {
        'ipAddrs': ['10.10.10.1/24']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1
    }
    r1 = net.addHost(name, cls=QuaggaRouter, quaggaConfFile=quaggaConf,
                     zebraConfFile=zebraConf, intfDict=intfs)

    # R2

    quaggaConf = '%sR2-quagga.conf' % CONFIG_DIR
    name = 'R2'
    eth0 = {
        'ipAddrs': ['150.1.1.1/30']
    }
    eth1 = {
        'ipAddrs': ['150.1.2.1/30']
    }
    eth2 = {
        'ipAddrs': ['20.0.0.1/8']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1,
        '%s-eth2' % name: eth2
    }
    r2 = net.addHost(name, cls=QuaggaRouter, quaggaConfFile=quaggaConf,
                     zebraConfFile=zebraConf, intfDict=intfs)

    # R3

    quaggaConf = '%sR3-quagga.conf' % CONFIG_DIR
    name = 'R3'
    eth0 = {
        'ipAddrs': ['150.1.2.2/30']
    }
    eth1 = {
        'ipAddrs': ['150.1.3.1/30']
    }
    eth2 = {
        'ipAddrs': ['30.0.0.1/8']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1,
        '%s-eth2' % name: eth2
    }
    r3 = net.addHost(name, cls=QuaggaRouter, quaggaConfFile=quaggaConf,
                     zebraConfFile=zebraConf, intfDict=intfs)

    # ExaBGP speaker

    name = 'exabgp'
    eth0 = {
        'ipAddrs': ['30.0.0.2/8']
    }
    eth1 = {
        'ipAddrs': ['192.168.1.1/24']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1
    }
    exabgp = net.addHost(name, cls=ExaBGPRouter,
                          exaBGPconf='%sexabgp.conf' % CONFIG_DIR,
                          intfDict=intfs)

    # ONOS edge OVS switch

    name = 'ovs'
    eth1 = {
        'ipAddrs': ['192.168.0.2/24']
    }
    intfs = {
        '%s-eth1' % name: eth1,
    }
    ovs = net.addSwitch(name, cls=ONOSSwitch,
                         dpid='00002a45d713e141', intfDict=intfs)

    # ONOS

    name = 'onos'
    eth0 = {
        'ipAddrs': ['192.168.0.1/24']
    }
    eth1 = {
        'ipAddrs': ['10.10.10.2/24']
    }
    eth2 = {
        'ipAddrs': ['192.168.1.2/24']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1,
        '%s-eth2' % name: eth2
    }
    onos = net.addHost(name, inNamespace=False, cls=Onos, intfDict=intfs)

    # Hosts

    h1 = net.addHost('h1', ip='10.0.0.100/8', defaultRoute='via 10.0.0.1')
    h2 = net.addHost('h2', ip='20.0.0.100/8', defaultRoute='via 20.0.0.1')
    h3 = net.addHost('h3', ip='30.0.0.100/8', defaultRoute='via 30.0.0.1')

    # Legacy Switch

    name = 's1'
    s1 = net.addSwitch(
        name, cls=LegacySwitch)

    # Legacy Switch

    name = 's2'
    s2 = net.addSwitch(
        name, cls=LegacySwitch)

    # Links

    net.addLink(h1, ovs, port1=0, port2=3)
    net.addLink(ovs, onos, port1=1, port2=0)
    net.addLink(ovs, r1, port1=4, port2=0)
    net.addLink(ovs, r2, port1=2, port2=0)
    net.addLink(r1, onos, port1=1, port2=1)
    net.addLink(r2, r3, port1=1, port2=0)
    net.addLink(onos, exabgp, port1=2, port2=1)
    net.addLink(r3, s1, port1=1, port2=1)

    net.addLink(r2, h2, port1=2, port2=0)

    net.addLink(r3, s2, port1=2)
    net.addLink(s2, h3)
    net.addLink(s2, exabgp, port2=0)

    net.start()

    s1.cmd('ovs-vsctl add-port s1 s1-gre1 -- set interface s1-gre1 type=gre options:remote_ip=' + REMOTE_IP)
    s1.cmdPrint('ovs-vsctl show')

    CLI(net)

    net.stop()

    info("done\n")
