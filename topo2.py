#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel, info, debug
from mininet.node import Host, RemoteController, OVSSwitch
import os

QUAGGA_DIR = '/usr/lib/quagga'
# Must exist and be owned by quagga user (quagga:quagga by default on Ubuntu)
QUAGGA_RUN_DIR = '/var/run/quagga'
EXABGP_RUN_EXE = '~/exabgp/sbin/exabgp'
CONFIG_DIR = 'configs/'

onos_controller = RemoteController('c0', ip='192.168.0.1', port=6633)


class Onos(Host):
    '''
        ONOS Host
    '''

    def __init__(self, name, intfDict, *args, **kwargs):
        Host.__init__(self, name, *args, **kwargs)

        self.intfDict = intfDict

    def config(self, **kwargs):
        Host.config(self, **kwargs)

        for intf, attrs in self.intfDict.items():
            self.cmd('ip addr flush dev %s' % intf)
            if 'mac' in attrs:
                self.cmd('ip link set %s down' % intf)
                self.cmd('ip link set %s address %s' % (intf, attrs['mac']))
                self.cmd('ip link set %s up ' % intf)
            for addr in attrs['ipAddrs']:
                self.cmd('ip addr add %s dev %s' % (addr, intf))


class QuaggaRouter(Host):
    '''
        Quagga Router Host
    '''

    def __init__(self, name, quaggaConfFile, zebraConfFile, intfDict, *args, **kwargs):
        Host.__init__(self, name, *args, **kwargs)

        self.quaggaConfFile = quaggaConfFile
        self.zebraConfFile = zebraConfFile
        self.intfDict = intfDict

    def config(self, **kwargs):
        Host.config(self, **kwargs)
        self.cmd('sysctl net.ipv4.ip_forward=1')

        for intf, attrs in self.intfDict.items():
            self.cmd('ip addr flush dev %s' % intf)
            if 'mac' in attrs:
                self.cmd('ip link set %s down' % intf)
                self.cmd('ip link set %s address %s' % (intf, attrs['mac']))
                self.cmd('ip link set %s up ' % intf)
            for addr in attrs['ipAddrs']:
                self.cmd('ip addr add %s dev %s' % (addr, intf))

        self.cmd('/usr/lib/quagga/zebra -d -f %s -z %s/zebra%s.api -i %s/zebra%s.pid' %
                 (self.zebraConfFile, QUAGGA_RUN_DIR, self.name, QUAGGA_RUN_DIR, self.name))
        self.cmd('/usr/lib/quagga/bgpd -d -f %s -z %s/zebra%s.api -i %s/bgpd%s.pid' %
                 (self.quaggaConfFile, QUAGGA_RUN_DIR, self.name, QUAGGA_RUN_DIR, self.name))

    def terminate(self):
        self.cmd("ps ax | egrep 'bgpd%s.pid|zebra%s.pid' | awk '{print $1}' | xargs kill" % (
            self.name, self.name))

        Host.terminate(self)


class ExaBGPRouter(Host):
    '''
        ExaBGP Router Host
    '''

    def __init__(self, name, exaBGPconf, intfDict, *args, **kwargs):
        Host.__init__(self, name, *args, **kwargs)

        self.exaBGPconf = exaBGPconf
        self.intfDict = intfDict

    def config(self, **kwargs):
        Host.config(self, **kwargs)
        self.cmd('sysctl net.ipv4.ip_forward=1')

        for intf, attrs in self.intfDict.items():
            self.cmd('ip addr flush dev %s' % intf)
            if 'mac' in attrs:
                self.cmd('ip link set %s down' % intf)
                self.cmd('ip link set %s address %s' % (intf, attrs['mac']))
                self.cmd('ip link set %s up ' % intf)
            for addr in attrs['ipAddrs']:
                self.cmd('ip addr add %s dev %s' % (addr, intf))

        self.cmd('%s %s > /dev/null 2> exabgp.log &' %
                 (EXABGP_RUN_EXE, self.exaBGPconf))

    def terminate(self):
        self.cmd(
            "ps ax | egrep 'lib/exabgp/application/bgp.py' | awk '{print $1}' | xargs kill")
        self.cmd(
            "ps ax | egrep 'server.py' | awk '{print $1}' | xargs kill")
        Host.terminate(self)


class ONOSSwitch(OVSSwitch):
    '''
        ONOS edge OVS switch
    '''

    def __init__(self, name, intfDict=None, **params):
        OVSSwitch.__init__(self, name, failMode='secure',
                           datapath='kernel', **params)
        self.intfDict = intfDict

    def start(self, controllers):
        for intf, attrs in self.intfDict.items():
            self.cmd('ip addr flush dev %s' % intf)
            if 'mac' in attrs:
                self.cmd('ip link set %s down' % intf)
                self.cmd('ip link set %s address %s' % (intf, attrs['mac']))
                self.cmd('ip link set %s up ' % intf)
            for addr in attrs['ipAddrs']:
                self.cmd('ip addr add %s dev %s' % (addr, intf))

        return OVSSwitch.start(self, [onos_controller])


class LegacySwitch(OVSSwitch):
    '''
        Legacy L2 switch
    '''

    def __init__(self, name, **params):
        OVSSwitch.__init__(self, name, failMode='standalone', **params)
        self.switchIP = None

    def start(self, controllers):
        return OVSSwitch.start(self, [])

if __name__ == '__main__':
    setLogLevel('debug')
    REMOTE_IP = '10.0.2.4'

    net = Mininet(topo=None, build=False)
    net.addController(onos_controller)

    # Legacy Switch

    name = 's3'
    s3 = net.addSwitch(
        name, cls=LegacySwitch)

    # R4

    zebraConf = '%szebra.conf' % CONFIG_DIR
    quaggaConf = '%sR4-quagga.conf' % CONFIG_DIR
    name = 'R4'
    eth0 = {
        'ipAddrs': ['150.1.4.2/30'],
        'mac': 'e2:f5:32:16:9a:46'
    }
    eth1 = {
        'ipAddrs': ['10.10.10.1/24']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1
    }
    r4 = net.addHost(name, cls=QuaggaRouter, quaggaConfFile=quaggaConf,
                     zebraConfFile=zebraConf, intfDict=intfs)

    # R8

    quaggaConf = '%sR8-quagga.conf' % CONFIG_DIR
    name = 'R8'
    eth0 = {
        'ipAddrs': ['150.1.3.2/30']
    }
    eth1 = {
        'ipAddrs': ['150.1.4.1/30']
    }
    eth2 = {
        'ipAddrs': ['150.1.6.1/30']
    }
    eth3 = {
        'ipAddrs': ['150.1.5.1/30']
    }
    eth4 = {
        'ipAddrs': ['80.0.0.1/8']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1,
        '%s-eth2' % name: eth2,
        '%s-eth3' % name: eth3,
        '%s-eth4' % name: eth4
    }
    r8 = net.addHost(name, cls=QuaggaRouter, quaggaConfFile=quaggaConf,
                     zebraConfFile=zebraConf, intfDict=intfs)

    # R6

    quaggaConf = '%sR6-quagga.conf' % CONFIG_DIR
    name = 'R6'
    eth0 = {
        'ipAddrs': ['150.1.6.2/30']
    }
    eth1 = {
        'ipAddrs': ['150.1.7.1/30']
    }
    eth2 = {
        'ipAddrs': ['60.0.0.1/8']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1,
        '%s-eth2' % name: eth2
    }
    r6 = net.addHost(name, cls=QuaggaRouter, quaggaConfFile=quaggaConf,
                     zebraConfFile=zebraConf, intfDict=intfs)

    # R7

    quaggaConf = '%sR7-quagga.conf' % CONFIG_DIR
    name = 'R7'
    eth0 = {
        'ipAddrs': ['150.1.7.2/30']
    }
    eth1 = {
        'ipAddrs': ['70.0.0.1/8']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1
    }
    r7 = net.addHost(name, cls=QuaggaRouter, quaggaConfFile=quaggaConf,
                     zebraConfFile=zebraConf, intfDict=intfs)

    # R5

    quaggaConf = '%sR5-quagga.conf' % CONFIG_DIR
    name = 'R5'
    eth0 = {
        'ipAddrs': ['150.1.5.2/30']
    }
    eth1 = {
        'ipAddrs': ['50.0.0.1/8']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1
    }
    r5 = net.addHost(name, cls=QuaggaRouter, quaggaConfFile=quaggaConf,
                     zebraConfFile=zebraConf, intfDict=intfs)

    # Hosts

    h4 = net.addHost('h4', ip='40.0.0.100/8', defaultRoute='via 40.0.0.1')
    h5 = net.addHost('h5', ip='50.0.0.100/8', defaultRoute='via 50.0.0.1')
    h6 = net.addHost('h6', ip='60.0.0.100/8', defaultRoute='via 60.0.0.1')
    h7 = net.addHost('h7', ip='70.0.0.100/8', defaultRoute='via 70.0.0.1')
    h8 = net.addHost('h8', ip='80.0.0.100/8', defaultRoute='via 80.0.0.1')

    # ONOS edge OVS switch

    name = 'ovs'
    eth1 = {
        'ipAddrs': ['192.168.0.2/24']
    }
    intfs = {
        '%s-eth1' % name: eth1,
    }
    ovs = net.addSwitch(name, cls=ONOSSwitch,
                         dpid='00002a45d713e141', intfDict=intfs)

    # ONOS

    name = 'onos'
    eth0 = {
        'ipAddrs': ['192.168.0.1/24']
    }
    eth1 = {
        'ipAddrs': ['10.10.10.2/24']
    }
    eth2 = {
        'ipAddrs': ['192.168.1.2/24']
    }
    intfs = {
        '%s-eth0' % name: eth0,
        '%s-eth1' % name: eth1,
        '%s-eth2' % name: eth2
    }
    onos = net.addHost(name, inNamespace=False, cls=Onos, intfDict=intfs)

    #Links

    net.addLink(r4, onos, port1=1, port2=1)
    net.addLink(onos, ovs, port1=0, port2=1)
    net.addLink(ovs, r4, port1=4, port2=0)
    net.addLink(ovs, r8, port1=2, port2=1)
    net.addLink(r8, s3, port1=0, port2=1)
    net.addLink(r8, r6, port1=2, port2=0)
    net.addLink(r8, r5, port1=3, port2=0)
    net.addLink(r6, r7, port1=1, port2=0)

    net.addLink(h4, ovs, port1=0, port2=3)
    net.addLink(h5, r5, port1=0, port2=1)
    net.addLink(h6, r6, port1=0, port2=2)
    net.addLink(h7, r7, port1=0, port2=1)
    net.addLink(h8, r8, port1=0, port2=4)

    net.start()

    s3.cmd('ovs-vsctl add-port s3 s3-gre1 -- set interface s3-gre1 type=gre options:remote_ip=' + REMOTE_IP)
    s3.cmdPrint('ovs-vsctl show')

    CLI(net)

    net.stop()

    info("done\n")
